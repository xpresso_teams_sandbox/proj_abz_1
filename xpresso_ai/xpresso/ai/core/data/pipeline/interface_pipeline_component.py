
__all__=["InterfacePipelineComponent"]
__author__ = "Srijan Sharma"

from abc import  ABC, abstractmethod

class InterfacePipelineComponent(ABC):
    """
    This interface represents a generic pipeline component.
    """
    def __init__(self):
        super().__init__()

    @abstractmethod
    def start(self,run_name):
        """
        Starts the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def terminate(self):
        """
        Stops the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def pause(self):
        """
        Pauses the component’s execution
        Returns:

        """
        pass

    @abstractmethod
    def restart(self):
        """
        Restarts the component’s execution
        Returns:

        """
        pass